import sirv from 'sirv';
import * as express from 'express';
import uuidv4 from 'uuid/v4';
import helmet from 'helmet';
import compression from 'compression';
import * as sapper from '@sapper/server';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

const app = express()

app.use(
	compression({ threshold: 0 }),
	sirv('static'),
)

// app.use((req, res, next) => {
// 	res.locals.nonce = uuidv4();
// 	next();
// });

// app.use(helmet({
// 	contentSecurityPolicy: {
// 		directives: {
// 			scriptSrc: [
// 				"'self'",
// 				(req, res) => `'nonce-${res.locals.nonce}'`
// 			]
// 		}
// 	}
// }));

app.use(sapper.middleware());

app.listen(PORT, err => {
	if (err) console.log('error', err);
});