import {writable} from 'svelte/store';

function pokemonStore(value = []) {
	const {update, subscribe} = writable(value);

	return {
		subscribe,
		addPokemonToStore: (pokemon) => update(old => [...old, pokemon]),
	};
}

export default pokemonStore();