import {writable} from 'svelte/store';

function userStore(value = {logged: false}) {
	const {set, subscribe} = writable(value);

	return {
		subscribe,
        signIn: (user) => set({logged: true, user}),
        signOut: () => set({logged: false}),
	};
}

export default userStore();