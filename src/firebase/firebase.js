import app from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyBfQI_mv4EXr0oyigcORWocY0b_FV4jy7g",
    authDomain: "sapper-pokedex.firebaseapp.com",
    databaseURL: "https://sapper-pokedex.firebaseio.com",
    projectId: "sapper-pokedex",
    storageBucket: "sapper-pokedex.appspot.com",
    messagingSenderId: "371040411794",
    appId: "1:371040411794:web:cb701965dd20f9cf"
};

const Firebase = () => {
      app.initializeApp(firebaseConfig);
      return app;
  }
  
  export default Firebase();